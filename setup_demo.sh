#!/usr/bin/env bash

## clear the working copy

    rm -rf gitdemo
    rm -rf gitdemo_oliver

## Arbeitsverzeichnis anlegen und GIT initiasieren

    mkdir gitdemo
    cd gitdemo
    git init .

## nun könnt ihr gitfork oder tortoise git show log starten


## ein erstes File erzeugen

    cat > firstfile.md <<`END`

    # this is my first file

    it has a paragraph for ralf
    which is pretty long

    it has a paragraph for bernhard
    which is pretty long

    it has a paragraph
    which is pretty long

    it has a paragraph
    which is pretty long

`END`

## 1. git status kontrollieren

    git status > status_01.txt

## 02 Das file nach zu git hinzufügen

    git add firstfile.md
    git status > status_02.txt

## 03 Die Änderung in git bestätigen (committen)

    git commit -m "03 master: my very first commit"
    git status > status_03.txt

## 04 ein zweites File erstellen

    cat > secondfile.md <<`END` \

    # this is my second file

    it has a paragraph for ralf
    which is pretty long

    it has a paragraph for bernhard
    which is pretty long

    it has a paragraph
    which is pretty long

    it has a paragraph
    which is pretty long

`END`

## 04 git status zeigt das as unversioniert an

    git status > status_04.txt

## 05 wir fügen es hinzu und bestätigen (committen) es

    git add secondfile.md
    git commit -m "05 master: my very second file"
    git status > status_05.txt

## 06 Nun ändern wir das erste File

    sed -i  -e 's/pretty/somewhat/g' firstfile.md
    
    git status > status_06.txt

## 07 Wir fügen die Änderung zu git hinzu und committen
    git add firstfile.md
    git commit -m "07 master: replace pretty by somewhat"
    git status > status_07.txt

## 08 nunn beginnt Bernhard seinen zweig für die die Parallelarbeit

    git checkout -b bernhard
    git status > status_08.txt

## 09 bernhard arbeitet nun auf seinem zweig

    sed -i  -e 's/bernhard/bernhard has changed/g' firstfile.md
    
    git add firstfile.md
    git commit -m "09 bernhard breplace bernhard by bernhard has changed"
    git status > status_09.txt


## 10 Ralf legt nun auch los

    git checkout -b ralf
    git status > status_10.txt

## 11 ralf arbeitet auf seinem zweig

    sed -i  -e 's/ralf/ralf has changed/g' firstfile.md
    
    git add firstfile.md
    git commit -m "11 ralf: replace ralf by ralf has changed"
    git status > status_11.txt

## 12 nun will sich bernhard mit ralf synchronisieren

    git checkout  bernhard
    git merge ralf -m "12 bernhard: merge the stuff from ralf to bernhard"
    git status > status_12.txt

## 13 Ralf entwickelt munter weiter

    git checkout  ralf
    sed -i bak -e 's/ralf has changed/ralf has changed again/g' firstfile.md
    
    git add firstfile.md
    git commit -m "13 ralf: half has changed again"
    git status > status_13.txt


## 14 auch Bernhard macht weiter

    sleep 2
    git checkout  bernhard
    sed -i  -e 's/bernhard has changed/bernhard has changed as well/g' firstfile.md
    
    git add firstfile.md
    git commit -m "14 bernhard: bernhard has changed as well"
    git status > status_14.txt


## 15 Ralf ist fertig und liefert in master ab

    sleep 2
    git checkout  ralf
    git merge master -m "15 ralf: merged master to ralf"
    git checkout master
    git merge ralf -m "15 master: now ralfs changes are in master"
    git status > status_15.txt


## 16 auch Bernhard liefert nun an Master

    sleep 2
    git checkout  bernhard
    git merge master -m "16 bernhard: merged master to bernhard"
    git checkout  master
    git merge bernhard -m "16 master: now bernhards changes are in master"
    git status > status_16.txt


## 17 nun arbeitet Bernhard weiter

    git checkout  bernhard
    sed -i  -e 's/ralf has changed again/ralf has changed once more/g' firstfile.md
    
    git add firstfile.md
    git commit -m "17 bernhard: change again to once more"
    git status > status_17.txt


## 18 Ralf hackt an der gleichen Stelle

    git checkout  ralf
    sed -i  -e 's/ralf has changed again/ralf has changed again and again/g' firstfile.md
    
    git add firstfile.md
    git commit -m "18 ralf: change again to again and again"
    git status > status_18.txt

## 19 Nun macht auch noch oliver mit, vorschriftsmässig im eigenen Verzeichnis

    cd ..
    git clone gitdemo gitdemo_oliver
    cd gitdemo_oliver
    git checkout  master
    git status > status_19.txt

## 20 Oliver macht ein Änderung

    git checkout -b  'oliver'
    sed -i  -e 's/ralf/oliver/g' secondfile.md
    
    git add secondfile.md
    git commit -m "20 oliver: has changed secondfile"

## 21 Oliver schiebt die Änderung rein "push"

    git push --set-upstream origin oliver
    cd ../gitdemo/
    git status > status_21.txt

## get out of here

    cd ..












