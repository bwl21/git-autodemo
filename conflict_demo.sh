## 23 Ralf hackt an der gleichen Stelle

    cd gitdemo
    git checkout ralf
    sed -i -e 's/ralf has changed again/ralf has changed again and again/g' firstfile.md

    git add firstfile.md
    git commit -m "23 ralf change again to again and again"
    git status > status_18.txt

## 24 als er sich synchronisieren will, gibt es einen Konflikt

    git merge bernhard -m "24 now we have a conflict once more and again and again"
    git status > status_19.txt

## Wir müssen den Konflikt manuell auflösen

